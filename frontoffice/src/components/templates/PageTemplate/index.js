import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Main } from 'components';

const PageTemplate = ({ header, aside, children }) => {
  return (
    <Fragment>
      <Fragment>{header}</Fragment>
      <Main>
        {children}
        <Fragment>{aside}</Fragment>
      </Main>
    </Fragment>
  );
};

PageTemplate.propTypes = {
  header: PropTypes.node.isRequired,
  aside: PropTypes.node,
  children: PropTypes.any.isRequired,
};
PageTemplate.defaultProps = {
  aside: null,
};

export default PageTemplate;
