import React from 'react';

import { Link } from 'components';
import Wrapper from './styles';

const Footer = props => {
  return (
    <Wrapper {...props}>
      <Link href="#">Footer | 2019</Link>
    </Wrapper>
  );
};

export default Footer;
