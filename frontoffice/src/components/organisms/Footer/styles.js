import styled from 'styled-components';

import theme from 'components/themes/main';
import { rem } from 'src/utils';

const { grey1 } = theme.colors;

const Wrapper = styled.footer`
  display: flex;
  justify-content: flex-end;
  background-color: ${grey1};
  padding: ${rem(20)};
`;

export default Wrapper;
