import styled from 'styled-components';

import theme from 'components/themes/main';

const { primaryDark } = theme.colors;

export const ModalOverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1040;
  width: 100vw;
  height: 100vh;
  background-color: ${primaryDark};
  opacity: 0.65;
`;
