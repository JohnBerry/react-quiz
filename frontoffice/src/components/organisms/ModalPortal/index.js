import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import { Modal, Paragraph } from 'components';

import { ModalOverlay } from './styles';

const ModalPortal = ({ show, hideModal, text, animation, children }) =>
  show
    ? ReactDOM.createPortal(
        <Fragment>
          <ModalOverlay />
          <Modal {...animation} hideModal={hideModal}>
            {text && <Paragraph content={text} />}
            {children}
          </Modal>
        </Fragment>,
        document.body
      )
    : null;

ModalPortal.propTypes = {
  show: PropTypes.bool.isRequired,
  hideModal: PropTypes.func.isRequired,
  text: PropTypes.any,
  children: PropTypes.any,
};

ModalPortal.defaultProps = {
  children: undefined,
  text: undefined,
};

export default ModalPortal;
