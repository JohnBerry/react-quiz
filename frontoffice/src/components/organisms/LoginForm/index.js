import React from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { Heading, Button, Field, Error } from 'components';
import { StyledForm, FormFooter } from './styles';

const Login = ({ onSubmitLogin, error }) => {
  const message = Object.values(error);

  return (
    <Formik
      initialValues={{
        email: '',
        password: '',
      }}
      validationSchema={Yup.object({
        email: Yup.string()
          .email('Invalid email address')
          .required('Email is required'),
        password: Yup.string().required('Password is required'),
      })}
      onSubmit={(values, { setSubmitting }) => {
        onSubmitLogin(values);
        setSubmitting(false);
      }}
    >
      <StyledForm>
        <Heading level={2}>Login</Heading>
        <Field name="email" type="email" label="Email" />
        <Field name="password" type="password" label="Password" />
        <FormFooter>
          <Error error={message} />
          <Button type="submit">Submit</Button>
        </FormFooter>
      </StyledForm>
    </Formik>
  );
};
Login.propTypes = {
  onSubmitLogin: PropTypes.func.isRequired,
  error: PropTypes.object,
};

Login.defaultProps = {
  error: {},
};

export default Login;
