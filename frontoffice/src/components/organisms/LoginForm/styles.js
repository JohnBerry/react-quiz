import styled from 'styled-components';
import { Form } from 'formik';

import theme from 'components/themes/main';
import { rem } from 'src/utils';

const { primary, dark2, grey5 } = theme.colors;
const { main, second } = theme.fonts;

const StyledForm = styled(Form)`
  position: relative;
  display: flex;
  flex-direction: column;
  border-radius: ${rem(5)};
  width: 25%;
  padding: ${rem(20)};
  box-shadow: 0 12px 25px 5px rgba(41, 48, 56, 0.4);
  background: ${dark2} url('src/assets/img/nav10.png') repeat center center/18px
    18px;

  h2 {
    text-align: center;
    color: ${grey5};
    font-family: ${main};
  }

  input[type='email'],
  input[type='password'] {
    border: none;
    border-bottom: 1px solid rgba(175, 238, 255, 0.35);
    font-family: ${second};
    color: ${grey5};

    &:focus {
      border-radius: ${rem(4, 4, 0, 0)};
      border-bottom: 1px solid ${primary};
    }
  }

  label {
    margin-top: ${rem(10)};
  }
`;

const FormFooter = styled.footer`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: ${rem(35)};

  button {
    display: flex;
    margin-left: auto;
  }

  .server-error + button {
    margin-left: 0;
  }

  .server-error {
    margin: 0;
    padding-right: ${rem(15)};
  }
`;

export { StyledForm, FormFooter };
