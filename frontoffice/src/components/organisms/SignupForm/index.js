import React from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { Heading, Button, Field, Error } from 'components';
import { StyledForm, FormFooter } from './styles';

const Signup = ({ onSubmitSignup, error }) => {
  const message = Object.values(error);

  return (
    <Formik
      initialValues={{
        nickname: '',
        email: '',
        password: '',
        confirmPassword: '',
      }}
      validationSchema={Yup.object({
        nickname: Yup.string()
          .max(15, 'Must be 15 characters or less')
          .required('Required'),
        email: Yup.string()
          .email('Invalid email address')
          .required('Email is required'),
        password: Yup.string()
          .min(6, 'Minimum 6 characters')
          .required('Password is required'),
        confirmPassword: Yup.string().oneOf(
          [Yup.ref('password'), null],
          "Don't match"
        ),
      })}
      onSubmit={(values, { setSubmitting }) => {
        onSubmitSignup(values);
        setSubmitting(false);
      }}
    >
      <StyledForm>
        <Heading level={2}>Signup</Heading>
        <Field name="nickname" label="Nickname" />
        <Field name="email" type="email" label="Email" />
        <Field name="password" type="password" label="Password" />
        <Field
          name="confirmPassword"
          type="password"
          label="Confirm password"
        />
        <FormFooter>
          <Error error={message} />
          <Button type="submit">Submit</Button>
        </FormFooter>
      </StyledForm>
    </Formik>
  );
};
Signup.propTypes = {
  onSubmitSignup: PropTypes.func.isRequired,
  error: PropTypes.object,
};

Signup.defaultProps = {
  error: {},
};

export default Signup;
