import styled from 'styled-components';
import { Paragraph } from 'components';

const IntroParagraph = styled(Paragraph)`
  width: 34%;
  text-align: center;
  margin-bottom: 1.75rem;
`;

export default IntroParagraph;
