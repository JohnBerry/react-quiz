import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import parse from 'html-react-parser';

import { Img, Heading, Button, Levels, ModalPortal } from 'components';
import {
  homePublicTitle,
  homePublicContent,
  homePublicCTA,
  homePrivateTitle,
  homePrivateContent,
  modalLoose,
  modalTimeout,
  modalExpired,
} from 'src/assets/data/en.json';
import { useEffectAsync } from 'src/utils';
import imgSrc from 'src/assets/svg/react.svg';
import IntroParagraph from './styles';

const Home = ({
  isAuthenticated,
  fetchLevels,
  levelsCategory,
  startQuiz,
  hideModal,
  modalName,
}) => {
  if (isAuthenticated) {
    useEffectAsync(() => fetchLevels(), []);
  }

  const loosingAnimation = {
    from: { transform: 'scale(0)' },
    to: { transform: 'scale(1)' },
    config: { tension: 300, friction: 9 },
  };

  const expiredAnimation = {
    from: { transform: 'translateY(-80%)', opacity: 0.2 },
    to: { transform: 'translateY(0)', opacity: 1 },
    config: { tension: 200, friction: 15 },
  };

  const modalProps = {
    loose: { message: modalLoose, animation: loosingAnimation },
    timeout: { message: modalTimeout, animation: loosingAnimation },
    expired: { message: modalExpired, animation: expiredAnimation },
  }[modalName] || { message: '', animation: null };
  const { message, animation } = modalProps;
  const showModal = message !== '';

  return (
    <Fragment>
      <Img name="React logo" src={imgSrc} width={250} />
      <Heading level={1}>
        {isAuthenticated ? homePrivateTitle : homePublicTitle}
      </Heading>
      <IntroParagraph
        content={
          isAuthenticated ? parse(homePrivateContent) : homePublicContent
        }
      />
      {!isAuthenticated && <Button href="/signup">{homePublicCTA}</Button>}
      <Levels
        isPrivate={isAuthenticated}
        levels={levelsCategory}
        handleClick={startQuiz}
      />
      <ModalPortal
        animation={animation}
        text={parse(message)}
        show={showModal}
        hideModal={hideModal}
      />
    </Fragment>
  );
};

Home.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  fetchLevels: PropTypes.func.isRequired,
  startQuiz: PropTypes.func.isRequired,
  levelsCategory: PropTypes.array.isRequired,
  hideModal: PropTypes.func.isRequired,
  modalName: PropTypes.string,
};

Home.defaultProps = {
  modalName: '',
};

export default Home;
