import styled from 'styled-components';

import theme from 'components/themes/main';

const { dark1 } = theme.colors;

const Wrapper = styled.header`
  display: flex;
  justify-content: flex-end;
  background: ${dark1} url('src/assets/img/nav10.png') repeat center center/18px
    18px;
  box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.4);
  position: relative;
  @media screen and (max-width: 640px) {
    padding: 0.5rem;
  }
`;

export default Wrapper;
