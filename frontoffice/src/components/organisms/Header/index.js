import React from 'react';

import { Nav } from 'containers';
import Wrapper from './styles';

const Header = ({ ...props }) => {
  return (
    <Wrapper {...props}>
      <Nav />
    </Wrapper>
  );
};

export default Header;
