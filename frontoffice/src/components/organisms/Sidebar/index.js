import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { List, SidebarItem, Countdown } from 'components';
import {
  scoreIcon,
  creditsIcon,
  extraTimeIcon,
  fiftyFiftyIcon,
  reviveIcon,
  skipIcon,
} from 'src/assets';
import { Wrapper, Hello, TimerWrapper } from './styles';

const Sidebar = ({
  isAuthenticated,
  user,
  jokers,
  score,
  isQuizInProgress,
  isJokerDisabled,
  duration,
  handleTimeout,
  ...props
}) => {
  const { nickname, credits } = user;
  const { extraTime, fiftyFifty, revive, skip } = jokers;

  return (
    <Fragment>
      {isAuthenticated && (
        <Wrapper {...props}>
          <Hello>Hello {nickname}!</Hello>
          <List>
            <SidebarItem item="points" count={score} svg={scoreIcon} score />
            <SidebarItem item="credits" count={credits} svg={creditsIcon} />
          </List>
          <List>
            <SidebarItem item="skip" count={skip} svg={skipIcon} />
            <SidebarItem item="revive" count={revive} svg={reviveIcon} />
            <SidebarItem
              item="fifty fifty"
              count={fiftyFifty}
              svg={fiftyFiftyIcon}
            />
            <SidebarItem
              item="extra time"
              count={extraTime}
              svg={extraTimeIcon}
            />
          </List>
          <TimerWrapper>
            <Countdown
              duration={duration}
              isQuizInProgress={isQuizInProgress}
              joker={isJokerDisabled}
              handleTimeout={handleTimeout}
            />
          </TimerWrapper>
        </Wrapper>
      )}
    </Fragment>
  );
};

Sidebar.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  isQuizInProgress: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired,
  score: PropTypes.number.isRequired,
  jokers: PropTypes.object,
  isJokerDisabled: PropTypes.object,
  duration: PropTypes.number,
  handleTimeout: PropTypes.func.isRequired,
};

Sidebar.defaultProps = {
  jokers: {},
  isJokerDisabled: {},
  duration: null,
};

export default Sidebar;
