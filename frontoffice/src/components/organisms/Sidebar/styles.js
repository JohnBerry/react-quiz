import styled from 'styled-components';

import theme from 'components/themes/main';
import { rem } from 'src/utils';

const { dark2 } = theme.colors;
const { main } = theme.fonts;

export const Wrapper = styled.aside`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  height: 100%;
  width: ${rem(240)};
  min-width: ${rem(240)};
  background: ${dark2} url('src/assets/img/nav10.png') repeat center center/18px
    18px;
`;

export const Hello = styled.header`
  font-family: ${main};
  font-size: ${rem(25)};
  border-bottom: 1px solid rgba(97, 218, 251, 0.2);
  padding: ${rem(20, 0)};
  margin: ${rem(0, 20)};
  text-align: center;
  color: #ffffff;
  font-weight: 400;
`;

export const TimerWrapper = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  background: transparent url('src/assets/svg/sidebar/chrono.svg') no-repeat
    center center/180px 180px;
`;
