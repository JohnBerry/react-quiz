import styled from 'styled-components';

import theme from 'components/themes/main';

const { dark2 } = theme.colors;

const StyledQuizHeader = styled.header`
  h1,
  h2 {
    color: ${dark2};
  }
`;

export default StyledQuizHeader;
