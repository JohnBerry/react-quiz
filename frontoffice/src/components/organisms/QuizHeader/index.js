import React from 'react';
import PropTypes from 'prop-types';

import { Heading } from 'components';
import StyledQuizHeader from './styles';

const QuizHeader = ({ question, breadcrumb, ...props }) => {
  return (
    <StyledQuizHeader {...props}>
      <Heading level={1}>{question}</Heading>
      <Heading level={2}>{breadcrumb}</Heading>
    </StyledQuizHeader>
  );
};

QuizHeader.propTypes = {
  question: PropTypes.string,
  breadcrumb: PropTypes.string.isRequired,
};

QuizHeader.defaultProps = {
  question: PropTypes.string,
};
export default QuizHeader;
