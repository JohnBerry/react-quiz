import styled, { css } from 'styled-components';

import theme from 'components/themes/main';
import { rem } from 'src/utils';

const { dark1 } = theme.colors;
const { main } = theme.fonts;

const styles = css`
  font-family: ${main};
  border-bottom: 1px solid rgba(97, 218, 251, 0.2);
  margin: ${rem(0, 20)};
  padding: ${rem(15, 0)};
  line-height: 1.7rem;
  color: ${dark1};
`;

export const Ol = styled.ol`
  ${styles}
`;
export const Ul = styled.ul`
  ${styles}
`;
