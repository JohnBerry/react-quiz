import React from 'react';
import PropTypes from 'prop-types';

import { Ol, Ul } from './styles';

const List = ({ ordered, children, ...props }) => {
  return React.createElement(ordered ? Ol : Ul, props, children);
};

List.propTypes = {
  children: PropTypes.any.isRequired,
  ordered: PropTypes.bool,
};

List.defaultProps = {
  ordered: false,
};

export default List;
