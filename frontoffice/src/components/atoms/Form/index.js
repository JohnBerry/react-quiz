import React from 'react';
import PropTypes from 'prop-types';
import StyledForm from './styles';

const Form = ({ submit, ...props }) => {
  return <StyledForm onSubmit={submit} {...props} />;
};

Form.propTypes = {
  submit: PropTypes.func.isRequired,
};

export default Form;
