import styled, { css } from 'styled-components';
import { NavLink } from 'react-router-dom';
import { rem } from 'src/utils';

import theme from 'components/themes/main';

const { primary, white } = theme.colors;
const { main, second } = theme.fonts;

const anchorLink = css`
  font-family: ${main};
  text-decoration: none;
  font-weight: 500;
  color: ${white};

  &:hover {
    text-decoration: underline;
  }
`;

export const StyledNavLink = styled(NavLink)`
  position: relative;
  font-family: ${second};
  text-decoration: none;
  font-size: ${rem(16)};
  padding: ${rem(15)};
  font-weight: 300;
  color: ${white};

  &:after {
    content: '';
    position: absolute;
    bottom: 0;
    background-color: ${primary};
    left: 0;
    border: transparent;
    height: 3px;
    width: 0;
    transition: width 0.2s ease;
  }

  &.active {
    color: ${primary};

    &:after {
      border: 1px solid ${primary};
      width: 100%;
    }
  }
  &:hover:after {
    left: auto;
    right: 0;
    border: 1px solid ${primary};
    width: 100%;
  }
`;

export const Anchor = styled.a`
  ${anchorLink}
`;
