import React from 'react';
import PropTypes from 'prop-types';

import { StyledNavLink, Anchor } from './styles';

const Link = ({ ...props }) => {
  const { to } = props;
  if (to) {
    return <StyledNavLink {...props} />;
  }
  return <Anchor {...props} />;
};

Link.propTypes = {
  to: PropTypes.string,
};

Link.defaultProps = {
  to: null,
};

export default Link;
