import styled from 'styled-components';

import theme from 'components/themes/main';

const { dark1 } = theme.colors;
const { second } = theme.fonts;

const StyledParagraph = styled.p`
  font-family: ${second};
  color: ${dark1};
  font-size: 1rem;
  line-height: 1.3;
  margin: 1rem 0 0;
`;

export default StyledParagraph;
