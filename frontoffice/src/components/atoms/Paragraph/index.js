import React from 'react';
import PropTypes from 'prop-types';

import StyledParagraph from './styles';

const Paragraph = ({ content, ...props }) => {
  return <StyledParagraph {...props}>{content}</StyledParagraph>;
};

Paragraph.propTypes = {
  content: PropTypes.any.isRequired,
};

export default Paragraph;
