import React from 'react';
import StyledListItem from './styles';

const ListItem = ({ ...props }) => {
  return <StyledListItem {...props} />;
};

export default ListItem;
