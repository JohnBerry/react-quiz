import styled from 'styled-components';
import { rem } from 'src/utils';

const StyledListItem = styled.li`
  display: flex;
  align-items: flex-end;
  padding: ${rem(15, 0)};
`;

export default StyledListItem;
