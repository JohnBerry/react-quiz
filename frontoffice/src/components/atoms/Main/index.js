import React from 'react';
import StyledMain from './styles';

const Main = ({ ...props }) => <StyledMain {...props} />;

export default Main;
