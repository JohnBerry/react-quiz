import styled from 'styled-components';

const StyledMain = styled.main`
  display: flex;
  flex: 1;
`;

export default StyledMain;
