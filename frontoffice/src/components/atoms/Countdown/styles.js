import styled from 'styled-components';

import theme from 'components/themes/main';
import { rem } from 'src/utils';

const { grey5 } = theme.colors;
const { main } = theme.fonts;

export const StyledCountdown = styled.div`
  display: flex;
  padding-top: ${rem(25)};
  font-family: ${main};
  align-items: center;
  font-size: ${rem(60)};
  color: ${grey5};
`;
