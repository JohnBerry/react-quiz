import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';

import { formattedTime } from 'src/utils';
import { useInterval } from 'src/hooks/useInterval';
import { StyledCountdown } from './styles';

const Countdown = ({ isQuizInProgress, duration, joker, handleTimeout }) => {
  const [time, setTime] = useState(duration);
  const extraDuration = time + Math.trunc(duration / 3.5);
  const [extraTime, setExtraTime] = useState(extraDuration);
  const [active, setActive] = useState(isQuizInProgress);

  const toggleTimeState = () =>
    joker.extraTime ? setExtraTime(extraTime - 1) : setTime(time - 1);
  const isExtratime = joker.extraTime ? extraTime : time;

  useInterval(
    () => {
      // Init extra time to run synchronously with regular time
      setExtraTime(extraTime - 1);
      if (isExtratime > 0) {
        toggleTimeState();
      } else {
        setActive(false);
        handleTimeout(isExtratime);
      }
    },
    1000,
    active
  );

  const minute = Math.trunc(isExtratime / 60);
  const second = isExtratime % 60;

  return (
    <Fragment>
      {isQuizInProgress && (
        <StyledCountdown>{formattedTime({ minute, second })}</StyledCountdown>
      )}
    </Fragment>
  );
};

Countdown.propTypes = {
  isQuizInProgress: PropTypes.bool.isRequired,
  duration: PropTypes.number,
  joker: PropTypes.object.isRequired,
  handleTimeout: PropTypes.func.isRequired,
};

Countdown.defaultProps = {
  duration: null,
};

export default Countdown;
