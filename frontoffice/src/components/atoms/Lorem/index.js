import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import uuidv4 from 'uuid/v4';

import StyledLorem from './styles';

const Lorem = ({ paragraph }) => {
  return (
    <Fragment>
      {[...Array(paragraph)].map(() => (
        <StyledLorem key={uuidv4()}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam euismod,
          risus eget cursus auctor, odio nisl condimentum dolor, quis lacinia
          urna augue sed nibh. Pellentesque nec est eu libero lobortis viverra.
          Suspendisse in fringilla lacus. Phasellus sit amet metus ac purus
          accumsan semper in sed libero. Cras sed dignissim mi, sed ullamcorper
          purus. Vestibulum venenatis aliquam leo, in dapibus odio sollicitudin
          et. Nullam egestas lacinia augue a hendrerit. Mauris vitae velit
          dapibus, auctor magna id, euismod orci. Nunc vel posuere libero.
          Vestibulum odio lorem, imperdiet eu dignissim ornare, elementum sit
          amet massa. Pellentesque sed nibh eros. Cras et elit non ligula
          rhoncus sollicitudin nec in nibh. Integer at urna at metus dignissim
          posuere vitae maximus magna. Suspendisse lacus tortor, lacinia id
          molestie ac, imperdiet ut est. Pellentesque auctor nulla eget libero
          lobortis, a fringilla odio feugiat. Etiam ultricies est libero, non
          tristique sapien condimentum quis. Quisque condimentum scelerisque
          nulla, sit amet vestibulum ex. Donec scelerisque, leo eget maximus
          ullamcorper, magna lorem fringilla nibh, quis dapibus ligula ex in
          leo. Ut sagittis finibus viverra. Ut consequat tempus nisi. Nullam
          commodo urna nisl, vel auctor nunc faucibus eget. Curabitur feugiat
          nibh nec volutpat accumsan. Sed vel odio tincidunt, aliquet leo id,
          commodo dolor. Aliquam erat volutpat. Ut venenatis eros sem. Praesent
          tincidunt sollicitudin viverra. Suspendisse luctus sollicitudin dolor
          vel cursus.
        </StyledLorem>
      ))}
    </Fragment>
  );
};

Lorem.propTypes = {
  paragraph: PropTypes.number.isRequired,
};

export default Lorem;
