import styled from 'styled-components';

import theme from 'components/themes/main';
import { rem } from 'src/utils';

const { dark1 } = theme.colors;
const { main } = theme.fonts;

const StyledLorem = styled.p`
  font-family: ${main};
  font-weight: 300;
  color: ${dark1};
  margin-bottom: ${rem(30)};
`;

export default StyledLorem;
