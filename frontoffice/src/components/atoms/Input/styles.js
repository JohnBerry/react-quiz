import styled, { css } from 'styled-components';
import { ifProp } from 'styled-tools';

import { rem } from 'src/utils';
import theme from 'components/themes/main';

const { main } = theme.fonts;
const { dark1, grey5 } = theme.colors;
const { danger } = theme.palette;

const styles = css`
  font-family: ${main};
  display: block;
  width: 100%;
  margin: 0;
  box-sizing: border-box;
  font-size: ${rem(15)};
  padding: ${ifProp(
    { type: 'textarea' },
    '0.4444444444rem',
    '0 0.4444444444rem'
  )};
  height: ${ifProp({ type: 'textarea' }, 'auto', '2.2222222222rem')};
  color: ${dark1};
  background-color: transparent;
  border: 1px solid ${ifProp('invalid', danger[2], grey5)};

  &[type='checkbox'],
  &[type='radio'] {
    display: inline-block;
    border: 0;
  }
`;

export const StyledTextarea = styled.textarea`
  ${styles}
`;
export const StyledSelect = styled.select`
  ${styles}
`;
export const StyledInput = styled.input`
  ${styles}
`;
