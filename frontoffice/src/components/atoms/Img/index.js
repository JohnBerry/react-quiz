import React from 'react';
import PropTypes from 'prop-types';
import StyledImg from './styles';

const Img = ({ name, ...props }) => {
  return <StyledImg title={name} alt={name} {...props} />;
};

Img.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Img;
