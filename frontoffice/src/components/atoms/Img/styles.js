import styled from 'styled-components';

const width = ({ widthProp }) => widthProp;
const StyledImg = styled.img`
  width: ${width};
`;

export default StyledImg;
