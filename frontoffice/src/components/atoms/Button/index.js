import React from 'react';
import PropTypes from 'prop-types';
import { StyledLink, Anchor, StyledButton } from './styles';

const Button = ({ type, ...props }) => {
  const { to, href } = props;
  if (to) {
    return <StyledLink {...props} />;
  }
  if (href) {
    return <Anchor {...props} />;
  }
  return <StyledButton {...props} type={type} />;
};

Button.propTypes = {
  height: PropTypes.number,
  type: PropTypes.string,
  to: PropTypes.string,
  href: PropTypes.string,
};

Button.defaultProps = {
  type: 'button',
  height: 40,
  to: undefined,
  href: undefined,
};

export default Button;
