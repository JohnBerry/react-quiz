import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';

import theme from 'components/themes/main';
import { rem } from 'src/utils';

const { primary, grey1 } = theme.colors;
const { second } = theme.fonts;

export const fontSize = ({ height }) => `${height / 40}rem`;

const styles = css`
  display: inline-flex;
  font-family: ${second};
  align-items: center;
  white-space: nowrap;
  font-size: ${fontSize};
  border: none;
  height: 2.5rem;
  justify-content: center;
  text-decoration: none;
  cursor: pointer;
  appearance: none;
  padding: ${rem(0, 15)};
  border-radius: ${rem(3)};
  background-color: ${grey1};
  color: ${primary};

  &:hover,
  &:focus,
  &:active {
    box-shadow: 0 1px 1px 0 rgba(2, 35, 42, 0.45),
      0 1px 3px 1px rgba(2, 35, 42, 0.3);
    transition: box-shadow 0.28s cubic-bezier(0.4, 0, 0.2, 1);
    cursor: pointer;
  }

  &:focus {
    outline: none;
  }
  &[disabled],
  &[disabled]:hover,
  &[disabled]:active {
    opacity: 0.3;
    cursor: not-allowed;
  }
`;

export const StyledLink = styled(Link)`
  ${styles}
`;

export const Anchor = styled.a`
  ${styles}
`;
export const StyledButton = styled.button`
  ${styles}
`;
