import React from 'react';
import PropTypes from 'prop-types';
import StyledError from './styles';

const Error = ({ formik, error }) => {
  return (
    <>
      {formik.touched && formik.error && (
        <StyledError role="alert">{formik.error}</StyledError>
      )}
      {error.length > 0 ? (
        <StyledError className="server-error" role="alert">
          {error}
        </StyledError>
      ) : null}
    </>
  );
};

Error.propTypes = {
  formik: PropTypes.object,
  error: PropTypes.array,
};

Error.defaultProps = {
  formik: {},
  error: [],
};

export default Error;
