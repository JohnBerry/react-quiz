import styled from 'styled-components';

import theme from 'components/themes/main';

const { danger } = theme.palette;
const { second } = theme.fonts;

const StyledError = styled.div`
  margin: 0.5rem 0 0;
  font-family: ${second};
  color: ${danger[1]};
`;

export default StyledError;
