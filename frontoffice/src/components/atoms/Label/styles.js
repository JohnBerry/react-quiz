import styled from 'styled-components';

import theme from 'components/themes/main';

const { grey5 } = theme.colors;
const { main } = theme.fonts;

const StyledLabel = styled.label`
  font-family: ${main};
  color: ${grey5};
  font-size: 1rem;
  line-height: 2em;
`;

export default StyledLabel;
