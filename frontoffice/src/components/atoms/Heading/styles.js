import { css } from 'styled-components';

import theme from 'components/themes/main';

const { dark1 } = theme.colors;
const { main } = theme.fonts;

const coeff = level => 1 / level;

export const fontSize = ({ level }) => `${1 + coeff(level)}rem`;

const headingStyles = css`
  font-family: ${main};
  font-weight: 500;
  font-size: ${fontSize};
  margin: 0;
  margin-top: 0.85714em;
  margin-bottom: 0.57142em;
  color: ${dark1};
`;

export default headingStyles;
