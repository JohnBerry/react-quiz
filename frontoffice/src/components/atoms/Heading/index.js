import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import headingStyles from './styles';

const StyledHeading = styled(({ level, children, ...props }) =>
  React.createElement(`h${level}`, props, children)
)`
  ${headingStyles}
`;

StyledHeading.propTypes = {
  level: PropTypes.number,
  children: PropTypes.node,

  reverse: PropTypes.bool,
};

StyledHeading.defaultProps = {
  level: 1,
};

const Heading = ({ ...props }) => {
  return <StyledHeading {...props} />;
};

export default Heading;
