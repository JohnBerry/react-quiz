const theme = {};

theme.colors = {
  primary: '#61dafb',
  primaryLight: '#afeeff',
  primaryDark: '#285d6b',
  dark1: '#20232a',
  dark2: '#293038',
  grey0: '#212121',
  grey1: '#414141',
  grey2: '#616161',
  grey3: '#9e9e9e',
  grey4: '#bdbdbd',
  grey5: '#e0e0e0',
  grey6: '#eeeeee',
  black: '#000000',
  white: '#ffffff',
};

theme.palette = {
  danger: ['#d32f2f', '#f44336', '#f8877f', '#ffcdd2'],
  alert: ['#ffa000', '#ffc107', '#ffd761', '#ffecb3'],
  success: ['#388e3c', '#4caf50', '#7cc47f', '#c8e6c9'],
};

theme.fonts = {
  main: "'Boogaloo', cursive",
  second: "'Roboto Condensed', sans-serif",
  backup: "'Helvetica Neue, Helvetica, Roboto', sans-serif",
  pre: 'Consolas, Liberation Mono, Menlo, Courier, monospace',
  quote: 'Georgia, serif',
};

theme.sizes = {
  maxWidth: '1100px',
};

export default theme;
