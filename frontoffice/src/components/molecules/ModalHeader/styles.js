import styled from 'styled-components';

import { rem } from 'src/utils';
import theme from 'components/themes/main';

const { grey5 } = theme.colors;
const { main } = theme.fonts;

export const Header = styled.header`
  display: flex;
  justify-content: flex-end;
  padding-bottom: ${rem(5)};
  width: 100%;

  button {
    background-color: transparent;
    padding: ${rem(10)};
    font-size: ${rem(25)};
    font-family: ${main};
    color: ${grey5};
    line-height: 0;

    &:hover,
    &:focus,
    &:active {
      box-shadow: 0 0 0 0 transparent;
    }
  }
`;
