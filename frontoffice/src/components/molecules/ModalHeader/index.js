import React from 'react';
import PropTypes from 'prop-types';

import { Button } from 'components';
import { Header } from './styles';

const ModalHeader = ({ handleHide }) => {
  return (
    <Header>
      <Button type="button" onClick={handleHide}>
        &times;
      </Button>
    </Header>
  );
};

ModalHeader.propTypes = {
  handleHide: PropTypes.func.isRequired,
};

export default ModalHeader;
