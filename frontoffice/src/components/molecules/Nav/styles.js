import styled from 'styled-components';

import theme from 'components/themes/main';
import { rem } from 'src/utils';

const { primary, white } = theme.colors;

const Navigation = styled.nav`
  display: flex;
  padding: ${rem(0, 10)};

  button {
    background-color: transparent;
    padding: ${rem(17, 15)};
    line-height: 1;
    color: ${white};

    &:hover {
      color: ${primary};
      transition: color 0.3s ease;
    }
  }
`;

export default Navigation;
