import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link, Button } from 'components';
import Navigation from './styles';

const Nav = ({ isAuthenticated, onLogout, ...props }) => {
  return (
    <Navigation role="navigation" {...props}>
      <Link to="/" exact activeClassName="active">
        Home
      </Link>
      {!isAuthenticated && (
        <Fragment>
          <Link to="/login" activeClassName="active">
            Login
          </Link>
          <Link to="/signup" activeClassName="active">
            Sign Up
          </Link>
        </Fragment>
      )}
      {isAuthenticated && (
        <Button type="button" onClick={onLogout}>
          Logout
        </Button>
      )}
    </Navigation>
  );
};

Nav.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  onLogout: PropTypes.func.isRequired,
};

export default Nav;
