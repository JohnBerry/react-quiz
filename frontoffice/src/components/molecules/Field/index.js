import React from 'react';
import PropTypes from 'prop-types';
import { useField } from 'formik';

import { Label, Input, Error } from 'components';
import { Wrapper, Block } from './styles';

const Field = ({ label, ...props }) => {
  const { id, name, type } = props;
  const [field, meta] = useField({ ...props });
  const renderInputFirst = type === 'checkbox' || type === 'radio';

  return (
    <Wrapper>
      {renderInputFirst && <Input {...field} {...props} />}
      <Block>
        {label && <Label htmlFor={id || name}>{label}</Label>}
        <Error formik={meta} />
      </Block>
      {renderInputFirst || <Input {...field} {...props} />}
    </Wrapper>
  );
};

Field.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  error: PropTypes.object,
};

Field.defaultProps = {
  label: undefined,
  id: undefined,
  type: 'text',
  error: {},
};

export default Field;
