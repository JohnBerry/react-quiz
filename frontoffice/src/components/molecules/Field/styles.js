import styled from 'styled-components';

import { rem } from 'src/utils';
import theme from 'components/themes/main';

const { danger } = theme.palette;
const { second } = theme.fonts;

const Error = styled.div`
  margin: 0.5rem 0 0;
  font-family: ${second};
  color: ${danger[1]};
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  label {
    display: block;
  }

  input {
    width: 100%;
    padding: ${rem(6)};

    &:focus {
      outline: none;
      background-color: rgba(225, 249, 255, 0.05);
    }
  }

  input[type='checkbox'],
  input[type='radio'] {
    margin-right: 0.5rem;
  }
`;
const Block = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: baseline;
  width: 100%;
  font-size: ${rem(15)};
`;

export { Error, Wrapper, Block };
