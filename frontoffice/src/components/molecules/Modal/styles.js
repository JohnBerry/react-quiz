import styled from 'styled-components';

import { rem } from 'src/utils';
import theme from 'components/themes/main';

const { dark2, primary } = theme.colors;
const { main } = theme.fonts;

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1050;
  width: 100%;
  height: 100%;
  overflow-x: hidden;
  overflow-y: auto;
  outline: 0;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  border-radius: ${rem(5)};
  box-shadow: 0 12px 25px 5px rgba(41, 48, 56, 0.4);
  background: ${dark2} url('src/assets/img/nav10.png') repeat center center/18px
    18px;
  width: 30%;
  max-width: 400px;

  p {
    flex: 1;
    margin: 0;
    padding: ${rem(0, 25, 25)};
    font-family: ${main};
    font-size: ${rem(27)};
    text-align: center;
    color: ${primary};
  }
`;
