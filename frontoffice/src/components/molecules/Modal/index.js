import React from 'react';
import PropTypes from 'prop-types';
import { Spring } from 'react-spring/renderprops';

import { ModalHeader } from 'components';
import { Wrapper, Content } from './styles';

const Modal = ({ hideModal, children, from, to, config }) => {
  return (
    <Wrapper onClick={hideModal}>
      <Spring from={from} to={to} config={config}>
        {props => (
          <Content style={props}>
            <ModalHeader handleHide={hideModal} />
            {children}
          </Content>
        )}
      </Spring>
    </Wrapper>
  );
};

Modal.propTypes = {
  children: PropTypes.any,
  hideModal: PropTypes.func.isRequired,
  from: PropTypes.object.isRequired,
  to: PropTypes.object.isRequired,
  config: PropTypes.object.isRequired,
};

Modal.defaultProps = {
  children: undefined,
};

export default Modal;
