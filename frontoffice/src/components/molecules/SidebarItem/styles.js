import styled from 'styled-components';

import theme from 'components/themes/main';
import { rem } from 'src/utils';

const { primary, white } = theme.colors;
const { second } = theme.fonts;

export const Count = styled.span`
  display: flex;
  align-items: flex-end;
  padding: ${rem(0, 10)};
  font-size: ${rem(32)};
  font-weight: 600;
  color: ${white};
`;

export const Text = styled.span`
  font-family: ${second};
  color: ${primary};
  font-weight: 300;
  font-size: ${rem(17)};
  line-height: 1;
`;
