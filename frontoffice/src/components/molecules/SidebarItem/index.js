import React from 'react';
import PropTypes from 'prop-types';
import { Icon, ListItem } from 'components';
import { Count, Text } from './styles';

const SidebarItem = ({ count, item, svg, score, ...props }) => {
  return (
    <ListItem {...props}>
      <Icon name={`icon ${item}`} src={svg} />
      <Count>
        {!score && `x`}
        {count}
      </Count>
      <Text>{item}</Text>
    </ListItem>
  );
};

SidebarItem.propTypes = {
  count: PropTypes.number,
  item: PropTypes.string.isRequired,
  svg: PropTypes.string.isRequired,
  score: PropTypes.bool,
};

SidebarItem.defaultProps = {
  score: false,
  count: null,
};

export default SidebarItem;
