import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'components';
import LevelsWrapper from './styles';

const Levels = ({ isPrivate, levels, handleClick, ...props }) => {
  return (
    <LevelsWrapper {...props}>
      {isPrivate &&
        levels.map(level => {
          return (
            <Button
              key={level}
              name={level}
              type="button"
              onClick={handleClick}
            >
              {level}
            </Button>
          );
        })}
    </LevelsWrapper>
  );
};

Levels.propTypes = {
  isPrivate: PropTypes.bool.isRequired,
  levels: PropTypes.array.isRequired,
  handleClick: PropTypes.func.isRequired,
};

export default Levels;
