import styled from 'styled-components';
import { rem } from 'src/utils';

const LevelsWrapper = styled.div`
  display: flex;
  padding: ${rem(0, 10)};

  button {
    margin: ${rem(5)};
  }
`;

export default LevelsWrapper;
