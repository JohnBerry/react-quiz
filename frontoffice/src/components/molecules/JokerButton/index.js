import React from 'react';
import PropTypes from 'prop-types';

import { Icon } from 'components';
import { StyledJokerButton } from './styles';

const JokerButton = ({ item, svg, children, ...props }) => {
  return (
    <StyledJokerButton name={item} {...props}>
      {svg && <Icon name={`icon ${item}`} src={svg} />}
      {children}
    </StyledJokerButton>
  );
};

JokerButton.propTypes = {
  svg: PropTypes.string,
  item: PropTypes.string,
  children: PropTypes.any,
};

JokerButton.defaultProps = {
  item: undefined,
  svg: undefined,
  children: undefined,
};

export default JokerButton;
