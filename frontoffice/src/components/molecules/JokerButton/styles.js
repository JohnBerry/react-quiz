import styled from 'styled-components';

import { Button } from 'components';
import theme from 'components/themes/main';
import { rem } from 'src/utils';

const { dark2 } = theme.colors;
const { main } = theme.fonts;

export const StyledJokerButton = styled(Button)`
  display: inline-flex;
  box-shadow: 0 10px 25px 0 rgba(41, 48, 56, 0.4);
  background: ${dark2} url('src/assets/img/nav10.png') repeat center center/18px
    18px;
  width: ${rem(60)};
  height: ${rem(60)};
  margin: ${rem(8)};
  font-family: ${main};

  img {
    pointer-events: none;
    width: ${rem(30)};
    height: ${rem(30)};
  }
`;
