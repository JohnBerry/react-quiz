import React from 'react';
import StyledIcon from './styles';

const Icon = ({ ...props }) => {
  return <StyledIcon {...props} />;
};

export default Icon;
