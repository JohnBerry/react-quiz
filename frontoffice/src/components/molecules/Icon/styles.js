import { Img } from 'components';
import styled from 'styled-components';
import { rem } from 'src/utils';

const StyledIcon = styled(Img)`
  width: ${rem(22)};
  height: ${rem(22)};
`;

export default StyledIcon;
