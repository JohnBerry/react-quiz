import styled from 'styled-components';

import { Input, Label } from 'components';
import theme from 'components/themes/main';
import { rem } from 'src/utils';

const { primary, dark2, grey5 } = theme.colors;
const { second } = theme.fonts;

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  margin-bottom: ${rem(25)};
  border-radius: ${rem(5)};
  box-shadow: 0 10px 25px 0 rgba(41, 48, 56, 0.4);
  background: ${dark2} url('src/assets/img/nav10.png') repeat center center/18px
    18px;
  padding: ${rem(0, 20)};
  font-size: ${rem(23)};
  animation: opacity 0.5s linear forwards;
  width: 100%;
`;

export const AnswerInput = styled(Input)`
  &[type='radio'] {
    position: absolute;
    top: 25px;
    left: 24px;
    width: ${rem(22)};
    height: ${rem(22)};
    opacity: 0;
    z-index: 1;
    cursor: pointer;
  }

  &:checked + label:after {
    content: '';
    position: absolute;
    top: 25px;
    left: 24px;
    background: ${primary};
    width: ${rem(22)};
    height: ${rem(22)};
    border-radius: ${rem(2)};
    box-shadow: 0 0 5px 2px rgba(97, 218, 251, 0.4);
  }

  &[type='text'] {
    margin: ${rem(15, 0)};
    padding: ${rem(7)};
    width: 100%;
    cursor: pointer;
    font-size: ${rem(23)};
    height: auto;
    border: none;
    border-bottom: 1px solid rgba(175, 238, 255, 0.35);
    color: ${grey5};
    font-family: ${second};

    &:focus {
      border-radius: ${rem(4, 4, 0, 0)};
      background-color: rgba(225, 249, 255, 0.05);
      border-bottom: 1px solid ${primary};
    }
  }
`;

export const AnswerLabel = styled(Label)`
  margin-left: ${rem(50)};
  width: 100%;
  padding: ${rem(20, 0)};
  text-align: left;
  cursor: pointer;
  font-family: ${second};

  &:before {
    content: '';
    position: absolute;
    top: 25px;
    left: 24px;
    background: ${grey5};
    width: ${rem(22)};
    height: ${rem(22)};
    border-radius: 2px;
  }
`;
