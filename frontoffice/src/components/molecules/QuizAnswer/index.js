import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { Wrapper, AnswerInput, AnswerLabel } from './styles';

const QuizAnswer = ({
  answers,
  answerType,
  onChangeAnswer,
  revive,
  inputSettings,
}) => {
  const type = answerType !== 'text';

  return (
    <Fragment>
      {answers.map(answer => {
        return (
          <Wrapper key={revive ? `${answer.answer} revive` : answer.answer}>
            <AnswerInput
              id={answer.answer}
              {...inputSettings}
              onChange={onChangeAnswer}
            />
            {type && (
              <AnswerLabel htmlFor={answer.answer}>{answer.answer}</AnswerLabel>
            )}
          </Wrapper>
        );
      })}
    </Fragment>
  );
};

QuizAnswer.propTypes = {
  answers: PropTypes.array.isRequired,
  onChangeAnswer: PropTypes.func.isRequired,
  answerType: PropTypes.string,
  inputSettings: PropTypes.object,
  revive: PropTypes.bool.isRequired,
};

QuizAnswer.defaultProps = {
  answerType: 'text',
  inputSettings: { type: 'radio', name: 'answer' },
};

export default QuizAnswer;
