export const pad = val => (val < 10 ? `0${val}` : val);

export const formattedTime = timeObj => {
  const { minute, second } = timeObj;

  return `${pad(minute)}:${pad(second)}`;
};
