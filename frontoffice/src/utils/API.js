import axios from 'axios';

export const AUTH_API = axios.create({
  baseURL: 'https://identitytoolkit.googleapis.com/v1',
});

export const API = axios.create({
  baseURL: 'https://vue-quizz-be217.firebaseio.com/',
});
