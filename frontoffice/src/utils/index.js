// UTILS EXPORT /////////////////////////////////////////////

const req = require.context('.', false, /^((?!index)[\s\S])*.js$/);

req.keys().forEach(key => {
  const utils = req(key);

  Object.keys(utils).forEach(name => {
    module.exports[name] = utils[name];
  });
});
