// PX TO REM CONVERTER /////////////////////////

export const rem = (px1, px2, px3, px4, basedFontSize = 16) => {
  // Filter undefined values
  const intValue = [px1, px2, px3, px4].filter(value => value !== undefined);
  // Remove commas and concat strings
  return intValue.map(value => `${value / basedFontSize}rem`).join(' ');
};
