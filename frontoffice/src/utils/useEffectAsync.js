import { useEffect } from 'react';
// React useEffect with async handler /////////////////////////

export const useEffectAsync = (effect, inputs) => {
  useEffect(() => {
    effect();
  }, inputs);
};

// https://stackoverflow.com/questions/53332321/react-hook-warnings-for-async-function-in-useeffect-useeffect-function-must-ret
