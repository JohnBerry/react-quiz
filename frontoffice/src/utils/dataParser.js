// JSON PARSER /////////////////////////

export const toJSON = data => JSON.parse(data);
export const toJS = data => JSON.stringify(data);
