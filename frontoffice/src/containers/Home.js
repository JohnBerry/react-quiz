import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
  fetchLevels,
  fetchQuestions,
  updateAsyncAccount,
} from 'src/store/operations';
import { setLevel, hideModal } from 'src/store/actions';
import { fromAuth, fromQuiz, fromModal } from 'src/store/selectors';
import { Home } from 'components';

const mapStateToProps = state => ({
  isAuthenticated: fromAuth.isAuthenticated(state),
  levelsCategory: fromQuiz.levelsCategory(state),
  modalName: fromModal.modalName(state),
});

const mapDispatchToProps = (dispatch, { history }) => ({
  fetchLevels: () => dispatch(fetchLevels()),
  startQuiz: event => {
    const { name } = event.target;
    dispatch(fetchQuestions(history));
    dispatch(setLevel(name));
    dispatch(updateAsyncAccount('credits'));
  },
  hideModal: () => dispatch(hideModal()),
});

const HomeContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);

export default withRouter(HomeContainer);
