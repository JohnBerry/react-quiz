import { connect } from 'react-redux';

import { AuthProvider } from 'src/hocs';
import { stayLogged } from 'src/store/operations';
import { fromAuth } from 'src/store/selectors';

const mapStateToProps = state => ({
  isAuthenticated: fromAuth.isAuthenticated(state),
});

const mapDispatchToProps = dispatch => ({
  stayLogged: () => dispatch(stayLogged()),
});

const AuthContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthProvider);

export default AuthContainer;
