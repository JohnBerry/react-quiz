import { connect } from 'react-redux';

import { CustomRoute } from 'src/hocs';
import { fromAuth } from 'src/store/selectors';

const mapStateToProps = state => ({
  isAuthenticated: fromAuth.isAuthenticated(state),
});

const CustomRouteContainer = connect(
  mapStateToProps,
  undefined
)(CustomRoute);

export default CustomRouteContainer;
