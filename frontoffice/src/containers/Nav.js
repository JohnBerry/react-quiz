import { connect } from 'react-redux';

import { onLogout } from 'src/store/operations';
import { fromAuth } from 'src/store/selectors';
import { Nav } from 'components';

const mapStateToProps = state => ({
  isAuthenticated: fromAuth.isAuthenticated(state),
});

const mapDispatchToProps = dispatch => ({
  onLogout: () => dispatch(onLogout()),
});

const NavContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Nav);

export default NavContainer;
