import { connect } from 'react-redux';

import { resetQuiz, showModal } from 'src/store/actions';
import { fromAuth, fromQuiz } from 'src/store/selectors';
import { Sidebar } from 'components';

const mapStateToProps = state => ({
  isAuthenticated: fromAuth.isAuthenticated(state),
  user: fromAuth.user(state),
  jokers: fromAuth.jokers(state),
  score: fromQuiz.score(state),
  isQuizInProgress: fromQuiz.isQuizInProgress(state),
  isJokerDisabled: fromQuiz.isJokerDisabled(state),
  duration: fromQuiz.duration(state),
});

const mapDispatchToProps = dispatch => ({
  handleTimeout: time => {
    dispatch(resetQuiz());
    if (time === 0) {
      dispatch(showModal('timeout'));
    }
  },
});

const SidebarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Sidebar);

export default SidebarContainer;
