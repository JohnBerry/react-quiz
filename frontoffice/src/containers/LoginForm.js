import { connect } from 'react-redux';

import { LoginForm } from 'components';
import { fromAuth } from 'src/store/selectors';
import { onSubmitLogin } from 'src/store/operations';

const mapStateToProps = state => ({
  error: fromAuth.error(state),
});

const mapDispatchToProps = dispatch => ({
  onSubmitLogin: values => {
    dispatch(onSubmitLogin(values));
  },
});

const LoginFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginForm);

export default LoginFormContainer;
