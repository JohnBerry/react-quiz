import { connect } from 'react-redux';

import { CustomRoute } from 'src/hocs';
import { fromQuiz } from 'src/store/selectors';

const mapStateToProps = state => ({
  isAuthenticated: fromQuiz.isQuizInProgress(state),
});

const QuizRouteContainer = connect(
  mapStateToProps,
  undefined
)(CustomRoute);

export default QuizRouteContainer;
