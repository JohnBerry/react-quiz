import { connect } from 'react-redux';

import { SignupForm } from 'components';
import { onSubmitSignup } from 'src/store/operations';

import { fromAuth } from 'src/store/selectors';

const mapStateToProps = state => ({
  error: fromAuth.error(state),
});

const mapDispatchToProps = dispatch => ({
  onSubmitSignup: values => {
    dispatch(onSubmitSignup(values));
  },
});

const SignupFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignupForm);

export default SignupFormContainer;
