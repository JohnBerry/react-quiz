import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
  answersMiddleware,
  submitMiddleware,
  updateAsyncAccount,
} from 'src/store/operations';
import {
  handleNextQuestion,
  disableJoker,
  setFiftyFifty,
  useJoker,
  resetQuiz,
} from 'src/store/actions';
import { fromQuiz } from 'src/store/selectors';
import { QuizPage } from 'pages';

const mapStateToProps = state => ({
  step: fromQuiz.step(state),
  stepLength: fromQuiz.stepLength(state),
  questionNumber: fromQuiz.questionNumber(state),
  question: fromQuiz.question(state),
  answers: fromQuiz.answers(state),
  answerType: fromQuiz.answerType(state),
  inputSettings: fromQuiz.inputSettings(state),
  answerValue: fromQuiz.answerValue(state),
  isJokerDisabled: fromQuiz.isJokerDisabled(state),
  isReviveUsed: fromQuiz.isReviveUsed(state),
});

const mapDispatchToProps = (dispatch, { history }) => ({
  handleAnswer: event => {
    dispatch(answersMiddleware(event));
  },
  handleSubmit: event => {
    event.preventDefault();
    dispatch(submitMiddleware(history));
  },
  handleJoker: event => {
    const { name } = event.target;

    switch (name) {
      case 'skip':
        dispatch(updateAsyncAccount('skip'));
        dispatch(handleNextQuestion());
        dispatch(disableJoker('skip'));
        break;

      case 'revive':
        dispatch(updateAsyncAccount('revive'));
        dispatch(disableJoker('revive'));
        break;

      case 'fifty fifty':
        dispatch(updateAsyncAccount('fiftyFifty'));
        dispatch(useJoker('isfiftyFiftyUsed'));
        dispatch(disableJoker('fiftyFifty'));
        break;

      case 'extra time':
        dispatch(updateAsyncAccount('extraTime'));
        dispatch(disableJoker('extraTime'));
        break;

      default:
        dispatch(resetQuiz());
        break;
    }
  },
  setFiftyFifty: () => dispatch(setFiftyFifty()),
});

const QuizContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(QuizPage);

export default withRouter(QuizContainer);
