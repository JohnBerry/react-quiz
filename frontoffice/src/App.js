import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';

import { CustomRoute, QuizRoute, AuthProvider, Quiz } from 'containers';
import { HomePage, LoginPage, SignupPage, NotFoundPage } from 'pages';
import GlobalStyle from './GlobalStyle';
import theme from './components/themes/main';

const App = () => {
  return (
    <AuthProvider>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <Switch>
          <Route path="/" component={HomePage} exact />
          <CustomRoute path="/login" component={LoginPage} />
          <CustomRoute path="/signup" component={SignupPage} />
          <QuizRoute isPrivate path="/quiz" component={Quiz} />
          <Route component={NotFoundPage} />
        </Switch>
      </ThemeProvider>
    </AuthProvider>
  );
};

export default App;
