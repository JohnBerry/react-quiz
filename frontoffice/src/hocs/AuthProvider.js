import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import AuthContext from './AuthContext';

const AuthProvider = ({ stayLogged, isAuthenticated, children }) => {
  useEffect(() => stayLogged());

  return (
    <AuthContext.Provider value={isAuthenticated}>
      {children}
    </AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  stayLogged: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
};

export default AuthProvider;
