import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

const CustomRoute = ({
  component: Component,
  isAuthenticated,
  isPrivate,
  ...rest
}) => {
  const togglePrivate = isPrivate ? !isAuthenticated : isAuthenticated;
  return (
    <Route
      {...rest}
      render={props => {
        return togglePrivate ? (
          <Redirect
            to={{
              pathname: '/',
              state: { from: props.location },
            }}
          />
        ) : (
          <Component {...props} />
        );
      }}
    />
  );
};

CustomRoute.propTypes = {
  component: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  isPrivate: PropTypes.bool,
  location: PropTypes.object.isRequired,
};
CustomRoute.defaultProps = {
  isPrivate: false,
};

export default CustomRoute;
