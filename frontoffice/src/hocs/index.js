// HOCS EXPORT /////////////////////////////////////////////

const req = require.context('.', false, /^((?!index).)*\.js$/);

req.keys().forEach(key => {
  const hocsName = key.replace(/^\.\/([^.]+)\.js$/, '$1');
  module.exports[hocsName] = req(key).default;
});
