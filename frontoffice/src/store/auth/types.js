// LOGIN TYPES ///////////////////////////////////////////

export const AUTH_USER = 'AUTH_USER';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const CLEAR_AUTH_USER = 'CLEAR_AUTH_USER';
export const DISCONNECT = 'DISCONNECT';
export const AUTH_ERROR = 'AUTH_ERROR';
export const UPDATE_ACCOUNT = 'UPDATE_ACCOUNT';
