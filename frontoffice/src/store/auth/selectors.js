import { createSelector } from 'reselect';

// Global auth selectors ========================================
export const isAuthenticated = state => state.auth.token !== null;
export const user = state => (state.auth.user ? state.auth.user : {});
export const jokers = createSelector(
  user,
  data => data.jokers
);
export const error = state => state.auth.error;
