import * as types from './types';

// LOGIN ACTIONS ///////////////////////////////////

export const authUser = (idToken, localId) => ({
  type: types.AUTH_USER,
  idToken,
  localId,
});

export const setCurrentUser = (user, userKey) => ({
  type: types.SET_CURRENT_USER,
  user,
  userKey,
});

export const clearAuthUser = () => ({
  type: types.CLEAR_AUTH_USER,
});

export const disconnect = () => ({
  type: types.DISCONNECT,
});

export const authError = error => ({
  type: types.AUTH_ERROR,
  error,
});

// USER ACCOUNT ACTIONS ///////////////////////////////////

export const updateAccount = name => ({
  type: types.UPDATE_ACCOUNT,
  name,
});
