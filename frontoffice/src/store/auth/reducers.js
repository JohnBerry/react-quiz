import * as types from './types';

const isToken = localStorage.getItem('token')
  ? localStorage.getItem('token')
  : null;

const initialState = {
  error: {},
  nickname: '',
  email: '',
  password: '',
  confirmPassword: '',
  token: isToken,
  userKey: null,
  user: {},
};

// LOGIN REDUCER ////////////////////////////////////

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case types.AUTH_USER:
      return {
        ...state,
        token: action.idToken,
        nickname: '',
        email: '',
        password: '',
        confirmPassword: '',
      };

    case types.AUTH_ERROR:
      return {
        ...state,
        error: action.error,
      };

    case types.SET_CURRENT_USER:
      return {
        ...state,
        user: action.user,
        userKey: action.userKey,
      };

    case types.CLEAR_AUTH_USER:
      return {
        ...state,
        token: null,
        user: {},
      };

    case types.UPDATE_ACCOUNT: {
      const { jokers } = state.user;
      const account =
        action.name === 'credits'
          ? { [action.name]: state.user[action.name] - 1 }
          : {
              jokers: {
                ...jokers,
                [action.name]: jokers[action.name] - 1,
              },
            };
      return {
        ...state,
        user: { ...state.user, ...account },
      };
    }

    default:
      return state;
  }
};
