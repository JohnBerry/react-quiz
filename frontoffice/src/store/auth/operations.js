import jwtDecode from 'jwt-decode';
import { API, AUTH_API } from 'src/utils';
import {
  authUser,
  setCurrentUser,
  clearAuthUser,
  updateAccount,
  authError,
  showModal,
} from 'src/store/actions';
import { apiKey } from '../../../local.config';

// FETCH CURRENT USER ////////////////////////////////////////////

export const fetchCurrentUser = (email, token) => {
  return async dispatch => {
    if (!token) {
      return;
    }

    try {
      const res = await API.get(
        `/users.json?orderBy="email"&equalTo="${email}"`
      );
      const user = Object.values(res.data)[0];
      const userKey = Object.keys(res.data)[0];

      dispatch(authUser(token));
      dispatch(setCurrentUser(user, userKey));
    } catch (error) {
      console.log('error', error);
    }
  };
};

// AUTO LOGIN/LOGOUT AND SIMPLE LOGOUT /////////////////////////////////////

export const onLogout = () => {
  return dispatch => {
    dispatch(clearAuthUser());
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('token');
  };
};

export const setLogoutTimer = expirationTime => {
  return dispatch => {
    setTimeout(() => {
      dispatch(onLogout());
      dispatch(showModal('expired'));
    }, expirationTime);
  };
};

export const stayLogged = () => {
  return (dispatch, getState) => {
    const token = localStorage.getItem('token');
    if (!token) {
      return;
    }

    const expirationDate = parseInt(localStorage.getItem('expirationDate'), 10);
    const now = new Date().getTime();
    if (now >= expirationDate) {
      dispatch(onLogout());
      dispatch(showModal('expired'));
      return;
    }

    const decodedToken = jwtDecode(token);
    const isUserEmpty = getState().auth.user;
    if (
      Object.entries(isUserEmpty).length === 0 &&
      isUserEmpty.constructor === Object
    ) {
      dispatch(fetchCurrentUser(decodedToken.email, token));
    }
  };
};

// LOGIN USER ////////////////////////////////////////////

export const onSubmitLogin = values => {
  return async dispatch => {
    const { email, password } = values;

    try {
      const res = await AUTH_API.post(
        `/accounts:signInWithPassword?key=${apiKey}`,
        {
          email,
          password,
          returnSecureToken: true,
        }
      );
      dispatch(fetchCurrentUser(email, res.data.idToken));

      const now = parseInt(new Date().getTime(), 10);
      const expirationDate = now + res.data.expiresIn * 1000;

      dispatch(setLogoutTimer(res.data.expiresIn * 1000));
      localStorage.setItem('token', res.data.idToken);
      localStorage.setItem('expirationDate', expirationDate);
    } catch (error) {
      const { message } = error.response.data.error;
      const field = {
        EMAIL_NOT_FOUND: { email: 'Email not found' },
        INVALID_PASSWORD: { password: 'Invalid password' },
      }[message] || { trylater: 'Too many attempt please try later...' };

      dispatch(authError(field));
    }
  };
};

// NEW USER REGISTER //////////////////////////////////////

export const storeSignupUser = (nickname, email, token) => {
  return async dispatch => {
    if (!token) {
      return;
    }

    const userBirth = new Date();
    const user = {
      created_at: userBirth,
      updated_time: userBirth,
      email,
      nickname,
      firstname: '',
      lastname: '',
      description: '',
      jokers: {
        fiftyFifty: 2,
        revive: 2,
        skip: 2,
        extraTime: 2,
      },
      credits: 2,
      score: 0,
    };

    try {
      await API.post(`/users.json?auth=${token}`, user);
      dispatch(fetchCurrentUser(email, token));
    } catch (error) {
      console.log('error', error);
    }
  };
};

export const onSubmitSignup = values => {
  return async dispatch => {
    const { email, password, nickname } = values;

    try {
      const res = await AUTH_API.post(`/accounts:signUp?key=${apiKey}`, {
        email,
        password,
        returnSecureToken: true,
      });
      dispatch(authUser(res.data.idToken, res.data.localId));
      dispatch(storeSignupUser(nickname, email, res.data.idToken));

      const now = new Date().getTime();
      const expirationDate = now + res.data.expiresIn * 1000;

      dispatch(setLogoutTimer(res.data.expiresIn * 1000));
      localStorage.setItem('token', res.data.idToken);
      localStorage.setItem('expirationDate', expirationDate);
    } catch (error) {
      const { message } = error.response.data.error;
      const field = {
        EMAIL_EXISTS: { email: 'Email already registered' },
      }[message] || { trylater: 'Too many attempt please try later...' };

      dispatch(authError(field));
    }
  };
};

// UPDATE PLAYER DATA //////////////////////////////////////

export const updateAsyncAccount = name => {
  return async (dispatch, getState) => {
    const { userKey, token, user } = getState().auth;
    const { currentScore } = getState().quiz;

    if (name !== 'score') dispatch(updateAccount(name));

    const account =
      name !== 'jokers'
        ? {
            path: `/users/${userKey}/${name}.json?auth=${token}`,
            put:
              name === 'credits' ? user[name] - 1 : user[name] + currentScore,
          }
        : {
            path: `/users/${userKey}/jokers/${name}.json?auth=${token}`,
            put: user.jokers[name] - 1,
          };

    try {
      await API.put(account.path, account.put);
    } catch (error) {
      console.log('error', error);
    }
  };
};
