import * as types from './types';

// MODALS ACTIONS
export const showModal = name => ({
  type: types.SHOW_MODAL,
  name,
});

export const hideModal = () => ({
  type: types.HIDE_MODAL,
});
