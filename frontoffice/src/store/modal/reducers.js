import * as types from './types';

const initialState = {
  name: '',
};

// QUIZ REDUCER ///////////////////////////////////////////////////////////////

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case types.SHOW_MODAL: {
      return {
        ...state,
        name: action.name,
      };
    }

    case types.HIDE_MODAL: {
      return {
        ...state,
        name: '',
      };
    }

    default:
      return state;
  }
};
