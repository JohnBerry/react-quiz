import * as types from './types';

const resetState = {
  data: {},
  level: {},
  step: 3,
  questionNumber: 4,
  answerValue: '',
  stepScore: 0,
  isJokerDisabled: {
    skip: false,
    revive: false,
    fiftyFifty: true,
    extraTime: false,
  },
  isReviveUsed: false,
  isfiftyFiftyUsed: false,
};

const initialState = {
  levels: [],
  currentScore: 0,
  ...resetState,
};

// QUIZ REDUCER ///////////////////////////////////////////////////////////////

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case types.STORE_LEVELS:
      return {
        ...state,
        levels: [...action.levels],
      };

    case types.STORE_QUIZ:
      return {
        ...state,
        data: { ...action.data },
      };

    case types.SET_LEVEL: {
      const levelData = state.levels.find(
        level => level.alias === action.level
      );

      return {
        ...state,
        level: { ...levelData },
      };
    }

    case types.HANDLE_NEXT_QUESTION: {
      const increment =
        state.questionNumber === 4
          ? { step: state.step + 1, questionNumber: 0 }
          : { questionNumber: state.questionNumber + 1 };
      const resetInputAnswer = state.step === 3 ? { answerValue: '' } : null;

      return {
        ...state,
        ...increment,
        ...resetInputAnswer,
      };
    }

    case types.UPDATE_SCORE: {
      const sum = state.currentScore + action.points;
      const updatePoints =
        state.questionNumber === 0 && state.step > 1
          ? { currentScore: sum, stepScore: sum }
          : { currentScore: sum };

      return {
        ...state,
        ...updatePoints,
      };
    }

    case types.UPDATE_ANSWER_VALUE:
      return {
        ...state,
        answerValue: action.value,
      };

    case types.RESET_QUIZ:
      return {
        ...state,
        ...resetState,
        currentScore: 0,
      };

    case types.WINNING_RESET:
      return {
        ...state,
        ...resetState,
      };

    case types.USE_JOKER: {
      const resetInputAnswer = state.step === 3 ? { answerValue: '' } : null;
      return {
        ...state,
        ...resetInputAnswer,
        [action.name]: !state[action.name],
      };
    }

    case types.SET_FIFTY_FIFTY: {
      return {
        ...state,
        isJokerDisabled: {
          ...state.isJokerDisabled,
          fiftyFifty: false,
        },
      };
    }

    case types.DISABLE_JOKER: {
      const targetedJoker = { [action.name]: true };
      return {
        ...state,
        isJokerDisabled: { ...state.isJokerDisabled, ...targetedJoker },
      };
    }

    default:
      return state;
  }
};
