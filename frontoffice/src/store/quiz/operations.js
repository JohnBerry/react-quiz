import { API } from 'src/utils';
import {
  storeLevels,
  storeQuiz,
  handleNextQuestion,
  updateAnswerValue,
  updateScore,
  resetQuiz,
  disableJoker,
  useJoker,
  setFiftyFifty,
  showModal,
  winningReset,
} from 'src/store/actions';
import { updateAsyncAccount } from 'src/store/operations';

// FETCH QUIZ OPERATIONS /////////////////////////////////////////////////

export const fetchLevels = () => {
  return async (dispatch, getState) => {
    const { token } = getState().auth;

    try {
      const res = await API.get(`/level.json?auth=${token}`);
      dispatch(storeLevels(res.data));
    } catch (error) {
      console.log('error', error);
    }
  };
};

export const fetchQuestions = history => {
  return async (dispatch, getState) => {
    const { token } = getState().auth;
    if (!token) {
      return;
    }

    try {
      const res = await API.get(`/questionsList.json?auth=${token}`);
      dispatch(storeQuiz(res.data));
      history.push('/quiz');
    } catch (error) {
      console.log('error', error);
    }
  };
};

// ANSWER OPERATIONS - RADIO INPUT AND SUBMIT FORM //////////////////////////

const answerOperations = (getState, dispatch, id, history) => {
  const {
    data,
    step,
    questionNumber,
    answerValue,
    level,
    isJokerDisabled,
    isReviveUsed,
    isfiftyFiftyUsed,
  } = getState().quiz;
  const path = data[`step${step}`][questionNumber];
  const { coeff } = level;
  const answerPath = path.answer;
  const answerPoints = path.points * coeff;
  const isReviveActive = isJokerDisabled.revive;
  const isFiftyFifty = isJokerDisabled.fiftyFifty;

  const dispatchNextQuestion = () => {
    // Jokers request =======================================
    // Enable fiftyfifty only on step 2...
    if (step === 1 && questionNumber === 4) {
      dispatch(setFiftyFifty());
    }
    // ...and disable it on step 3 if it is not already done
    if (step === 2 && questionNumber === 4 && !isFiftyFifty) {
      dispatch(disableJoker('fiftyFifty'));
    }
    // Stop filtering answers on next question
    if (isfiftyFiftyUsed) {
      dispatch(useJoker('isfiftyFiftyUsed'));
    }

    // Winning quiz =======================================
    if (step === 3 && questionNumber === 4) {
      dispatch(updateScore(answerPoints));
      dispatch(updateAsyncAccount('score'));
      dispatch(winningReset());
      history.push('/');
    } else {
      // Default =======================================
      dispatch(handleNextQuestion());
      dispatch(updateScore(answerPoints));
    }
  };

  // The quiz ends if revive joker has been used
  const toggleReset = () => {
    if (isReviveActive && !isReviveUsed) {
      setTimeout(() => {
        dispatch(useJoker('isReviveUsed'));
      }, 150);
    } else {
      dispatch(resetQuiz());
      dispatch(showModal('loose'));
    }
  };

  // Toggle correct answer condition between radio and text input
  let isCorrect;

  if (step !== 3) {
    isCorrect = answerPath.find(el => el.answer === id).is_correct;
    // Add delay for the UX, without the user won't see the checked radio
    return isCorrect
      ? setTimeout(() => {
          dispatchNextQuestion();
        }, 150)
      : toggleReset();
  }

  isCorrect = answerPath[0].answer.toLowerCase() === answerValue.toLowerCase();
  // Here delay isn't needed with the input text
  return isCorrect ? dispatchNextQuestion() : toggleReset();
};

export const answersMiddleware = event => {
  return (dispatch, getState) => {
    const { id, type, value } = event.target;

    // onChange input operations between radio and text
    if (type === 'radio') {
      answerOperations(getState, dispatch, id);
    } else {
      dispatch(updateAnswerValue(value));
    }
  };
};

export const submitMiddleware = history => {
  return (dispatch, getState) => {
    answerOperations(getState, dispatch, { id: null }, history);
  };
};
