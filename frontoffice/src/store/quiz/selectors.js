import { createSelector } from 'reselect';
import { fromAuth } from 'src/store/selectors';
import { shuffle } from 'src/utils';

// Levels ========================================
export const levelsCategory = state =>
  state.quiz.levels.map(level => level.alias);

export const duration = state => state.quiz.level.duration;

// Global quiz data ===========================================
const quizData = state => state.quiz.data;

// Is quiz in progress ? ======================================
export const isQuizInProgress = createSelector(
  quizData,
  fromAuth.isAuthenticated,
  (data, auth) => Object.keys(data).length > 0 && auth
);

// Question breadcrumbs =======================================
export const step = state => state.quiz.step;
export const questionNumber = state => state.quiz.questionNumber;
export const stepLength = createSelector(
  quizData,
  step,
  (data, stepInt) => data[`step${stepInt}`].length
);

// Jokers ==================================================
export const isJokerDisabled = state => state.quiz.isJokerDisabled;
export const isReviveUsed = state => state.quiz.isReviveUsed;
export const isfiftyFiftyUsed = state => state.quiz.isfiftyFiftyUsed;

// Question and answers =======================================
const questionData = target =>
  createSelector(
    quizData,
    step,
    questionNumber,
    (data, stepInt, questionInt) => data[`step${stepInt}`][questionInt][target]
  );
export const question = questionData('question');
export const answersData = questionData('answer');

export const answers = createSelector(
  answersData,
  step,
  isfiftyFiftyUsed,
  (array, stepInt, filter) => {
    if (stepInt === 2 && filter) {
      return shuffle([
        ...array.filter(item => item.is_correct === true),
        array.filter(item => item.is_correct === false)[0],
      ]);
    }
    return array;
  }
);

// Toggle Input settings between radio and text type ===========
const isLastStep = state => state.quiz.step === 3;
export const answerType = createSelector(
  isLastStep,
  value => (value ? 'text' : 'radio')
);
export const answerValue = state => state.quiz.answerValue;
export const inputSettings = createSelector(
  answerType,
  answerValue,
  (answer, value) =>
    answer === 'text'
      ? {
          type: answer,
          name: 'answer',
          autoComplete: 'off',
          autoFocus: 'on',
          value,
        }
      : {
          type: answer,
          name: 'answer',
        }
);

// UpdatedScore ==================================================
export const score = createSelector(
  state => state.quiz.currentScore,
  fromAuth.user,
  (quiz, db) => quiz + db.score
);
