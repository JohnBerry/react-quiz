import * as types from './types';

// FETCHING DATA
export const storeLevels = levels => ({
  type: types.STORE_LEVELS,
  levels,
});

export const storeQuiz = data => ({
  type: types.STORE_QUIZ,
  data,
});

export const setLevel = level => ({
  type: types.SET_LEVEL,
  level,
});

// QUIZ
export const handleNextQuestion = () => ({
  type: types.HANDLE_NEXT_QUESTION,
});

export const updateScore = points => ({
  type: types.UPDATE_SCORE,
  points,
});

export const updateAnswerValue = value => ({
  type: types.UPDATE_ANSWER_VALUE,
  value,
});

export const resetQuiz = () => ({
  type: types.RESET_QUIZ,
});

export const winningReset = () => ({
  type: types.WINNING_RESET,
});

// JOKERS
export const disableJoker = name => ({
  type: types.DISABLE_JOKER,
  name,
});

export const useJoker = name => ({
  type: types.USE_JOKER,
  name,
});

export const setFiftyFifty = () => ({
  type: types.SET_FIFTY_FIFTY,
});
