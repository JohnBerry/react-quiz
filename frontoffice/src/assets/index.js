// ASSETS GLOBAL EXPORT ////////////////////////////////////////

// SVG =========================================================
export const scoreIcon = 'src/assets/svg/sidebar/score.svg';
export const creditsIcon = 'src/assets/svg/sidebar/credits.svg';
export const extraTimeIcon = 'src/assets/svg/sidebar/extraTime.svg';
export const fiftyFiftyIcon = 'src/assets/svg/sidebar/fiftyFifty.svg';
export const reviveIcon = 'src/assets/svg/sidebar/revive.svg';
export const skipIcon = 'src/assets/svg/sidebar/skip.svg';
