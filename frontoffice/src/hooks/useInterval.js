import { useEffect, useRef } from 'react';

// Hooks from Dan Abramov explanation about setInterval:
// https://overreacted.io/making-setinterval-declarative-with-react-hooks/
// And modified from here:
// https://codesandbox.io/s/github/leo-khramtsov/stopwatch-countdown-react-hooks/tree/master/

export const useInterval = (callback, delay, active) => {
  const savedCallback = useRef();
  const intervalId = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (active && delay !== null) {
      const id = setInterval(tick, delay);
      intervalId.current = id;
      return () => clearInterval(intervalId.current);
    }

    return (() => {
      clearInterval(intervalId.current);
      console.log('clear');
    })();
  }, [delay, active]);
};
