import styled from 'styled-components';

import { Img } from 'components';
import theme from 'components/themes/main';
import { rem } from 'src/utils';

const { maxWidth } = theme.sizes;

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  height: 100vh;
`;

const Content = styled.section`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  margin: ${rem(40)} auto;
  max-width: ${maxWidth};
`;

const Logo = styled(Img)`
  width: 100%;
  margin: ${rem(40)} auto;
  max-width: ${maxWidth};
`;

export { PageWrapper, Content, Logo };
