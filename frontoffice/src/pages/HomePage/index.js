import React from 'react';

import { PageTemplate, Header } from 'components';
import { Home, Sidebar } from 'containers';
import { PageWrapper, Content } from './styles';

const HomePage = () => {
  return (
    <PageWrapper>
      <PageTemplate header={<Header />} aside={<Sidebar />}>
        <Content>
          <Home />
        </Content>
      </PageTemplate>
    </PageWrapper>
  );
};

export default HomePage;
