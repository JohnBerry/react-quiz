import React from 'react';

import { PageTemplate, Header } from 'components';
import { LoginForm } from 'containers';
import { PageWrapper, Content } from './styles';

const LoginPage = () => {
  return (
    <PageWrapper>
      <PageTemplate header={<Header />}>
        <Content>
          <LoginForm />
        </Content>
      </PageTemplate>
    </PageWrapper>
  );
};

export default LoginPage;
