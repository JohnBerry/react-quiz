import HomePage from './HomePage';
import LoginPage from './LoginPage';
import NotFoundPage from './NotFoundPage';
import QuizPage from './QuizPage';
import SignupPage from './SignupPage';

export { HomePage, LoginPage, QuizPage, NotFoundPage, SignupPage };
