import React from 'react';

import { PageTemplate, Header } from 'components';
import { SignupForm } from 'containers';
import { PageWrapper, Content } from './styles';

const SignupPage = () => {
  return (
    <PageWrapper>
      <PageTemplate header={<Header />}>
        <Content>
          <SignupForm />
        </Content>
      </PageTemplate>
    </PageWrapper>
  );
};

export default SignupPage;
