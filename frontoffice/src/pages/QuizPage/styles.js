import styled from 'styled-components';

import { Form, JokerButton } from 'components';
import theme from 'components/themes/main';
import { rem } from 'src/utils';

const { maxWidth } = theme.sizes;

export const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  height: 100vh;
`;

export const StyledForm = styled(Form)`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin: ${rem(40)} auto;
  max-width: ${maxWidth};

  h1 {
    margin-top: ${rem(70)};
  }
`;

export const Fieldset = styled.fieldset`
  width: 100%;
  padding: ${rem(0, 30)};
  margin: 0;
`;

export const QuizFooter = styled.footer`
  display: flex;
  align-items: center;
  margin-bottom: ${rem(50)};
`;

export const JokerStopButton = styled(JokerButton)`
  width: ${rem(95)};
  height: ${rem(75)};
  font-size: ${rem(32)};
  padding-bottom: ${rem(3)};
`;
