import React from 'react';
import PropTypes from 'prop-types';

import {
  PageTemplate,
  Header,
  QuizHeader,
  QuizAnswer,
  JokerButton,
} from 'components';
import { Sidebar } from 'containers';
import {
  extraTimeIcon,
  fiftyFiftyIcon,
  reviveIcon,
  skipIcon,
} from 'src/assets';
import {
  PageWrapper,
  StyledForm,
  Fieldset,
  QuizFooter,
  JokerStopButton,
} from './styles';

const QuizPage = ({
  step,
  stepLength,
  questionNumber,
  question,
  answers,
  answerType,
  answerValue,
  handleAnswer,
  handleSubmit,
  handleJoker,
  inputSettings,
  isJokerDisabled,
  isReviveUsed,
}) => {
  const { skip, revive, fiftyFifty, extraTime } = isJokerDisabled;

  return (
    <PageWrapper>
      <PageTemplate header={<Header />} aside={<Sidebar />}>
        <StyledForm submit={handleSubmit}>
          <QuizHeader
            question={question}
            breadcrumb={`Step ${step} - Question ${questionNumber +
              1} / ${stepLength}`}
          />
          <Fieldset>
            <QuizAnswer
              answers={answers}
              answerType={answerType}
              onChangeAnswer={handleAnswer}
              inputSettings={inputSettings}
              value={answerValue}
              revive={isReviveUsed}
            />
          </Fieldset>
          <QuizFooter>
            <JokerButton
              disabled={skip}
              item="skip"
              svg={skipIcon}
              onClick={handleJoker}
            />
            <JokerButton
              disabled={revive}
              item="revive"
              svg={reviveIcon}
              onClick={handleJoker}
            />
            <JokerStopButton item="stop" onClick={handleJoker}>
              Stop!
            </JokerStopButton>
            <JokerButton
              disabled={fiftyFifty}
              item="fifty fifty"
              svg={fiftyFiftyIcon}
              onClick={handleJoker}
            />
            <JokerButton
              disabled={extraTime}
              item="extra time"
              svg={extraTimeIcon}
              onClick={handleJoker}
            />
          </QuizFooter>
        </StyledForm>
      </PageTemplate>
    </PageWrapper>
  );
};

QuizPage.propTypes = {
  step: PropTypes.number.isRequired,
  stepLength: PropTypes.number.isRequired,
  questionNumber: PropTypes.number.isRequired,
  question: PropTypes.string,
  answers: PropTypes.array,
  answerType: PropTypes.string.isRequired,
  answerValue: PropTypes.string,
  handleAnswer: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleJoker: PropTypes.func.isRequired,
  inputSettings: PropTypes.object,
  isJokerDisabled: PropTypes.object.isRequired,
  isReviveUsed: PropTypes.bool.isRequired,
};

QuizPage.defaultProps = {
  question: null,
  answers: [],
  inputSettings: null,
  answerValue: '',
};
export default QuizPage;
