# react-quiz

React-quiz is like a personal "todolist like" training project transpose into a real playable quizz! It's based on the atomic design philosophy and use recent React updates like 'Hooks' but also Redux for the global state. The API is managed by firebase.
The live hosted demo is coming soon, it's still a work in progress!

## Signup

![subscribe](screenshot_signup.jpg)

## Private Home

![subscribe](screenshot_home.jpg)

## Quiz

![subscribe](screenshot_quiz.jpg)
